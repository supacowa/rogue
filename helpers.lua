

-- This are global helper functions and classes used by the game














-- check if given value is a integer or float (lua number) value
function isNumber(v)
    return v and type(v) == "number"
end








-- Check if given number is an integer (not a float)
function isInteger(n)
    return n and isNumber(n) and n == math.floor(n)
end








-- Check if given value is of type table
function isTable(value)
    return value and type(value) == "table"
end








function isFunc(value)
    return value and type(value) == "function"
end









-- If a variable is nil then choose default-boolean otherwise take variables boolean value
-- NOTE `return bool or default` will not work in this case because it would use default on both nil and false but we want only override the nil!
function defaultBoolean(bool, default)
    return type(bool) == "nil" and default or bool
end









-- Clamps a number to within a certain range [n, m]
function clamp(n, low, high)
    return math.min(math.max(low or 0, n), high or 1)
end








-- Map value from one range to another
function remapRange(val, a1, a2, b1, b2)
    return b1 + (val-a1) * (b2-b1) / (a2-a1)
end











-- Linear interpolation between two numbers
function lerp(from, to, fraction)
    local f = clamp(fraction)
    return (1 - f) * from + f * to
end



function lerp2(from, to, fraction)
    return from + (to - from) * clamp(fraction)
end


-- Cosine interpolation between two numbers
function cerp(from, to, fraction)
    local f = (1 - math.cos(clamp(fraction) * math.pi)) * .5
    return from * (1 - f) + to * f
end




















-- Get the position of a point on screen as a dynamic percentage value [0-1]
function pixelPositionToPercentage(point_x, point_y)
    local width, height = love.window.getMode()
    return point_x / width, point_y / height
end



-- Get the pixel position on screen from a percentage value [0-1]
function percentPositionToPixel(dyn_x, dyn_y)
    local width, height = love.window.getMode()
    return width * dyn_x, height * dyn_y
end























-- Prettified output of tables

function printf(t, indent)
    if not indent then indent = "" end

    local names = {}

    for n, g in pairs(t) do
        table.insert(names, n)
    end

    table.sort(names)
    
    for i, n in pairs(names) do
        local v = t[n]
        if isTable(v) then
            if v == t then -- prevent infinite loop on self reference
                print(indent..tostring(n)..": <-")
            else
                print(indent..tostring(n)..":")
                printf(v, indent.."   ")
            end
        elseif type(v) == "function" then
            print(indent..tostring(n).."()")
        else
            print(indent..tostring(n)..": "..tostring(v))
        end
    end
end






















function cloneTable(src, dst) -- deep-copy
    if not isTable(src) then
        return t
    end

    dst = dst or {}
    local meta = getmetatable(src)

    for k, v in pairs(src) do
        if type(v) == "table" then
            dst[k] = cloneTable(v, dst)
        else
            dst[k] = v
        end
    end

    setmetatable(dst, meta)

    return dst
end






















-- Show debug information like fps, frame-rate and lua ram usage
function debugger() -- pivot on screen edge 0-1
    collectgarbage()

    local _, h = love.window.getMode()
    local info = string.format(
        "framerate: %.3fms \nfrequency: %ifps \nmemory: %.0fkb",
        1000 * love.timer.getDelta(),
        math.floor(1 / love.timer.getDelta()),
        collectgarbage("count")
    )

    love.graphics.push("all")
    love.graphics.printf(info, 10, h - 50, 200, "left")
    love.graphics.pop()
end



























do
    -- https://codea.io/talk/discussion/96/class-inheritance

    -- Class system by @Simeon from TTL
    -- Compatible with Lua 5.1 (not 5.0)


    function class(base)
        local c = {}    -- a new class instance
        if isTable(base) then
            -- our new class is a shallow copy of the base class!
            for i,v in pairs(base) do
                c[i] = v
            end
            c._base = base
        end

        -- the class will be the metatable for all its objects,
        -- and they will look up their methods in it.
        c.__index = c

        -- expose a constructor which can be called by <classname>( <args> )
        local mt = {}

        mt.__call = function(class_tbl, ...)
            local obj = {}
            setmetatable(obj,c)

            if class_tbl.init then
                class_tbl.init(obj,...)
            else 
                -- make sure that any stuff from the base class is initialized!
                if base and base.init then
                    base.init(obj, ...)
                end
            end

            return obj
        end

        c.is_a = function(self, klass)
            local m = getmetatable(self)
            while m do 
                if m == klass then return true end
                m = m._base
            end
            return false
        end

        setmetatable(c, mt)
        return c
    end
end





















do
    -- http://hump.readthedocs.io/en/latest/index.html

    --[[
    Copyright (c) 2010-2013 Matthias Richter

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    Except as contained in this notice, the name(s) of the above copyright holders
    shall not be used in advertising or otherwise to promote the sale, use or
    other dealings in this Software without prior written authorization.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
    --]]


    local assert, sqrt, cos, sin, atan2 = assert, math.sqrt, math.cos, math.sin, math.atan2
    
    vec2 = class()


    
    function vec2:init(x, y) -- default constructor
        self.y = y or x or 0
        self.x = x or 0
    end

    function vec2:fromPolar(angle, radius) -- another type constructor (e.g. local vector = vec2():fromPolar(90, 64))
        return vec2(cos(angle) * radius, sin(angle) * radius)
    end

    function vec2:isVector()
        return isTable(self) and isNumber(self.x) and isNumber(self.y)
    end

    function vec2:clone()
        return vec2(self.x, self.y)
    end

    function vec2:unpack()
        return self.x, self.y
    end

    function vec2:__tostring()
        return "("..tonumber(self.x)..", "..tonumber(self.y)..")"
    end

    function vec2.__unm(a)
        return vec2(-a.x, -a.y)
    end

    function vec2.__add(a, b)
        assert(vec2.isVector(a) and vec2.isVector(b), "Add: wrong argument types (<vector> expected)")
        return vec2(a.x + b.x, a.y + b.y)
    end

    function vec2.__sub(a, b)
        assert(vec2.isVector(a) and vec2.isVector(b), "Sub: wrong argument types (<vector> expected)")
        return vec2(a.x - b.x, a.y - b.y)
    end

    function vec2.__mul(a, b)
        if isNumber(a) then
            return vec2(a * b.x, a * b.y)
        elseif isNumber(b) then
            return vec2(b * a.x, b * a.y)
        else
            assert(vec2.isVector(a) and vec2.isVector(b), "Mul: wrong argument types (<vector> or <number> expected)")
            return a.x * b.x + a.y * b.y
        end
    end

    function vec2.__div(a, b)
        assert(vec2.isVector(a) and type(b) == "number", "wrong argument types (expected <vector> / <number>)")
        return vec2(a.x / b, a.y / b)
    end

    function vec2.__eq(a, b)
        return a.x == b.x and a.y == b.y
    end

    function vec2.__lt(a, b)
        return a.x < b.x or (a.x == b.x and a.y < b.y)
    end

    function vec2.__le(a, b)
        return a.x <= b.x and a.y <= b.y
    end

    function vec2.permul(a, b)
        assert(vec2.isVector(a) and vec2.isVector(b), "permul: wrong argument types (<vector> expected)")
        return vec2(a.x * b.x, a.y * b.y)
    end

    function vec2:toPolar()
        return vec2(atan2(self.x, self.y), self:len())
    end

    function vec2:len2()
        return self.x * self.x + self.y * self.y
    end

    function vec2:len()
        return sqrt(self.x * self.x + self.y * self.y)
    end

    function vec2.dist(a, b)
        assert(vec2.isVector(a) and vec2.isVector(b), "dist: wrong argument types (<vector> expected)")
        local dx = a.x - b.x
        local dy = a.y - b.y
        return sqrt(dx * dx + dy * dy)
    end

    function vec2.dist2(a, b)
        assert(vec2.isVector(a) and vec2.isVector(b), "dist: wrong argument types (<vector> expected)")
        local dx = a.x - b.x
        local dy = a.y - b.y
        return (dx * dx + dy * dy)
    end

    function vec2:normalizeInplace()
        local len = self:len()
        if len > 0 then
            self.x, self.y = self.x / len, self.y / len
        end
        return self
    end

    function vec2:normalized()
        return self:clone():normalizeInplace()
    end

    function vec2:rotateInplace(phi)
        local c, s = cos(phi), sin(phi)
        self.x, self.y = c * self.x - s * self.y, s * self.x + c * self.y
        return self
    end

    function vec2:rotated(phi)
        local c, s = cos(phi), sin(phi)
        return vec2(c * self.x - s * self.y, s * self.x + c * self.y)
    end

    function vec2:perpendicular()
        return vec2(-self.y, self.x)
    end

    function vec2:projectOn(v)
        assert(vec2.isVector(v), "invalid argument: cannot project vector on " .. type(v))
        -- (self * v) * v / v:len2()
        local s = (self.x * v.x + self.y * v.y) / (v.x * v.x + v.y * v.y)
        return vec2(s * v.x, s * v.y)
    end

    function vec2:mirrorOn(v)
        assert(vec2.isVector(v), "invalid argument: cannot mirror vector on " .. type(v))
        -- 2 * self:projectOn(v) - self
        local s = 2 * (self.x * v.x + self.y * v.y) / (v.x * v.x + v.y * v.y)
        return vec2(s * v.x - self.x, s * v.y - self.y)
    end

    function vec2:cross(v)
        assert(vec2.isVector(v), "cross: wrong argument types (<vector> expected)")
        return self.x * v.y - self.y * v.x
    end

    -- ref.: http://blog.signalsondisplay.com/?p=336
    function vec2:trimInplace(maxLen)
        local s = maxLen * maxLen / self:len2()
        s = (s > 1 and 1) or math.sqrt(s)
        self.x, self.y = self.x * s, self.y * s
        return self
    end

    function vec2:angleTo(other)
        if other then
            return atan2(self.y, self.x) - atan2(other.y, other.x)
        end
        return atan2(self.y, self.x)
    end

    function vec2:trimmed(maxLen)
        return self:clone():trimInplace(maxLen)
    end

end






















do
    -- This is a convenience class for working with colors
    -- Works similar to Codea

    -- TODO add methods for mixing/adding/substracting and converting colors if needed
    

    color = class()


    function color:init(r, g, b, a)
        -- NOTE this class stores rgba values in normalized form [0-1]
        -- if you pass in values from 0-255 they will be converted to 0-1

        -- set defaults
        local red = r or 1
        local green = not b and red or (g or red)
        local blue = b or red
        local alpha = not b and (a or g or 1) or (a or 1)
        
        -- convert if needed and store the values
        self:setRGBA(red, green, blue, alpha)
    end


    function color:normalize(r, g, b, a)
        assert(r and g and b and a, "To normalize a color supply all four arguments for r,g,b,a")

        r = clamp(r, 0, 255)
        g = clamp(g, 0, 255)
        b = clamp(b, 0, 255)
        a = clamp(a, 0, 255)

        return
            r > 1 and r/255 or r,
            g > 1 and g/255 or g,
            b > 1 and b/255 or b,
            a > 1 and a/255 or a
    end


    function color:setRGBA(r, g, b, a)
        self.r, self.g, self.b, self.a = self:normalize(r, g, b, a)
    end


    function color:getRGBA()
        return
            self.r * 255,
            self.g * 255,
            self.b * 255,
            self.a * 255
    end


    function color:isColor()
        return isTable(self) and isNumber(self.r) and isNumber(self.g) and isNumber(self.b) and isNumber(self.a)
    end


    function color:__tostring()
        return "("..tonumber(self.r)..", "..tonumber(self.g)..", "..tonumber(self.b)..", "..tonumber(self.a)..")"
    end


    function color:unpack()
        return self.r, self.g, self.b, self.a
    end

end




























--
-- Ported this piece from Codea, although this one might be an overkill feature for Love2D because it works simpler
--
-- Gather uv information about any rectangular region (set of tiles) on a texture
-- Get a sequence of all region-rects from i to j where each sub-region is a tile of width x height
-- The 'explicit'-flag returns only tiles enclosed by the overall region from i to j (skipping the appendices and in-betweens)
-- Regions are described by their index position on texture - reading from top left corner on texture, indices are: 1,2,3...n
-- i and j indices might also be passed as vec2(col, row) which is convenient when spritesheet dimensions grow over time and where sprite indices might shift
--


function uvTexture(texture, region_width, region_height, i, j, explicit)
    local texture_width, texture_height = texture:getDimensions()
    local cols = texture_width / region_width
    local rows = texture_height / region_height
    
    -- Get sprite index from col and row
    local function getId(cell)
        assert(vec2.isVector(cell), "Bad optional parameters to uvTexture() – Region limits from i to j must be numbers or vectors!")
        return (cell.y - 1) * cols + cell.x
    end
    
    -- Get col and row from sprite index
    local function getCell(id)
        local rem = id % cols
        local col = (rem ~= 0 and rem or cols) - 1
        local row = math.ceil(id / cols) - 1
        return col, row
    end
    
    i = i and (isNumber(i) and i or getId(i)) or 1 -- be sure to deal always with number indices
    j = j and (isNumber(j) and j or getId(j)) or i
    
    local minCol, minRow = getCell(i)
    local maxCol, maxRow = getCell(j)
    local tiles = {}
    local region = {}
    
    -- Collect all tiles enclosed by i and j
    for k = i, j do
        local col, row = getCell(k)
        local w = 1 / cols
        local h = 1 / rows
        local u = w * col
        local v = h * row
        
        if not explicit
        or (col >= minCol and col <= maxCol)
        then
            table.insert(tiles, {
                id = k, -- region rect index on spritesheet
                col = col + 1, -- example: tile at {col = 1, row = 1}
                row = row + 1, -- beginning at the top left corner of the spritesheet
                x = col * region_width, -- {x, y} is the lower left corner position of the tile at {col, row}
                y = row * region_height,
                width = region_width,
                height = region_height,
                uv = {
                    x1 = u,
                    y1 = v,
                    x2 = u + w,
                    y2 = v + h,
                    w = w,
                    h = h
                }
            })
        end
    end
    
    -- Sort tiles by column and row in ascending order
    table.sort(tiles, function(curr, list)
        return curr.row == list.row and curr.col < list.col or curr.row < list.row
    end)
    
    -- Describe the overall region-rect
    local region = {
        x = tiles[1].x,
        y = tiles[1].y,
        width = tiles[#tiles].x + tiles[#tiles].width - tiles[1].x,
        height = tiles[#tiles].y + tiles[#tiles].height - tiles[1].y,
        uv = {
            x1 = tiles[1].uv.x1,
            y1 = tiles[1].uv.y1,
            x2 = tiles[#tiles].uv.x2,
            y2 = tiles[#tiles].uv.y2,
            w = tiles[#tiles].uv.x2 - tiles[1].uv.x1,
            h = tiles[#tiles].uv.y2 - tiles[1].uv.y1
        }
    }
    
    return region, tiles
end

























do
    -- Create a textured and animated mesh quad
    -- Note: available animations are listed as pairs e.g. name = {list of frames as vec2}
    --
    -- @params {}:
    --
    -- texture: image
    -- tilesize: vec2
    -- spritesize: vec2
    -- position: vec2
    -- pivot: vec2 [0-1]
    -- animations: {}
    -- current_animation: "string"
    -- fps: number
    -- loop: boolean
    -- tintcolor: color()
    --
    -- example:
    --[[
    {
        texture = love.graphics.newImage("image.png"),
        tilesize = vec2(8, 8),
        spritesize = vec2(32, 32),
        position = vec2(0, 0),
        pivot = vec2(0, 0),
        animations = {
            default = {vec2(1, 1)}
        },
        current_animation = "default",
        fps = 24,
        loop = true,
        tintcolor = color(255)
    }
    --]]


    Sprite = class()


    function Sprite:init(params)

        -- copy all given params
        for name, prop in pairs(params) do
            self[name] = prop
        end

        -- override presets
        self.tintcolor = self.tintcolor or color()
        self.spritesize = self.spritesize or self.tilesize
        self.position = self.position or vec2()
        self.pivot = self.pivot or vec2()
        self.current_frame = 1
        self.fps = self.fps or 24
        self.loop = defaultBoolean(self.loop, true)
        self.visible = defaultBoolean(self.visible, true)

        self.quad = love.graphics.newQuad(0, 0, 0, 0, self.texture:getDimensions())
        self.texture:setFilter("linear", "nearest")
    end


    function Sprite:draw(time_scale)
        if not self.timer or self.timer <= love.timer.getTime() then
            local anim = self.animations[self.current_animation]
            local frm = self.current_frame
            local uv = uvTexture(self.texture, self.tilesize.x, self.tilesize.y, anim[frm])
            self.quad:setViewport(uv.x, uv.y, uv.width, uv.height)

            local delay = clamp(time_scale or 1) * self.fps -- inf at 0 time_scale
            self.timer = love.timer.getTime() + 1 / delay
            self.current_frame = anim[frm + 1] and frm + 1 or 1
            
            if frm == #anim and not self.loop then
                self.current_frame = frm -- pull back
            end
        end

        if self.visible then
            love.graphics.push("all")
            love.graphics.translate(self.position.x - self.pivot.x * self.spritesize.x, self.position.y - self.pivot.y * self.spritesize.y)
            love.graphics.scale(self.spritesize.x / self.tilesize.x, self.spritesize.y / self.tilesize.y)
            love.graphics.setColor(self.tintcolor:unpack())
            love.graphics.draw(self.texture, self.quad)
            love.graphics.pop()
        end
    end

end




























-- This is a collection of UI components to create windows, buttons, text and other graphical goodies

do -- UI BLOCK


    Container = class()


    function Container:init(x, y, width, height)
        local w, h = percentPositionToPixel(.75, .5)

        -- presets for background types
        self.FILL = self.drawBackground
        self.SKIN = self.drawSkin

        -- presets for background properties
        self.DEFAULT = {
            type = self.FILL, -- draw function
            spritesheet = love.graphics.newImage("images/ui-containers.png"), -- TODO cache the spritesheet globally somehow so that each class can reference instead of loading its own copy
            tile_width = 8,
            tile_height = 8,
            scale = 4,
            color = color(40, 0), -- additiv on skins
            radius = 4
        }

        -- presets for constraints (4 column layout)
        self.GRID_1 = 1
        self.GRID_1_2 = .5
        self.GRID_1_3 = .333
        self.GRID_1_4 = .25


        -- entity properties
        self.x = x or 0
        self.y = y or 0
        self.pivot = vec2()
        self.width = width or w
        self.height = height or h
        self.background = self.DEFAULT
        self.visible = false

        -- NOTE margins have higher priority than regular constraints!
        -- in a way margins are static (strict) types of constraints
        -- margins define the exact pixel distance from the window edge
        -- (that's why a window will grow if margins become larger than the window and why the window can never be smaller than two opposite margins)
        -- while regular (dynamic) constraints work by percentage values [0-1] and adjust automatically to points floating on screen

        self.margin_left = nil
        self.margin_top = nil
        self.margin_right = nil
        self.margin_bottom = nil

        self.constraint_left = nil
        self.constraint_top = nil
        self.constraint_right = nil
        self.constraint_bottom = nil

        self.overflow = false -- or clip & scroll

        self:show()
    end




    function Container:show()
        -- NOTE in an actual case implimentation you may want to override/extend the *:show() and *:hide() methods to freeze/continue the gameplay
        self.visible = true
    end




    function Container:hide()
        self.visible = false
    end




    function Container:clamp(width, height)
        -- NOTE (untested) implemented this method with mobile devices in mind (portrait, landscape orientation)
        -- basically it cuts container's width and height to screen dimensions

        local w, h = love.window.getMode()
        width = width or w
        height = height or h

        if self.width > width or self.height > height then
            self.width = clamp(self.width, 0, width)
            self.height = clamp(self.height, 0, height)
        end
    end



    function Container:getMargins()
        -- NOTE if any container's edge is off-screen the returned margin for it will be negative!

        -- calculate container's true edge positions without any constraints while accounting for the pivot offset
        local w, h = love.window.getMode()
        local left = self.x - self.pivot.x * self.width
        local top = self.y - self.pivot.y * self.height
        local right = w - (left + self.width)
        local bottom = h - (top + self.height)

        -- return already constrained margins or newly calculated ones
        return
            self.margin_left or left,
            self.margin_top or top,
            self.margin_right or right,
            self.margin_bottom or bottom
    end




    function Container:setMargins(margin_left, margin_top, margin_right, margin_bottom)
        -- NOTE margin argument of nil will turn off that constraint!

        self.margin_left = margin_left
        self.margin_top = margin_top
        self.margin_right = margin_right
        self.margin_bottom = margin_bottom
    end




    function Container:getConstraints(from_edge_left, from_edge_top, from_edge_right, from_edge_bottom)
        -- NOTE this are not current constraints but rather predicted (future) constraints!
        -- the container's margins for these calculations are either based on given values, the current constraints (if any margins were set already) or based on constainer's floating position and size on screen
        -- NOTE constraints can be negative! See *:getMargin() method for more information

        local l, t, r, b = self:getMargins()
        local left, top = pixelPositionToPercentage(from_edge_left or l, from_edge_top or t)
        local right, bottom = pixelPositionToPercentage(from_edge_right or r, from_edge_bottom or b)

        return left, top, right, bottom
    end




    function Container:setConstraints(edge_left, edge_top, edge_right, edge_bottom)
        -- NOTE margin argument of nil will turn off the constraint!

        local left, top, right, bottom = self:getConstraints(edge_left, edge_top, edge_right, edge_bottom)

        if edge_left then self.constraint_left = left else self.constraint_left = nil end
        if edge_top then self.constraint_top = top else self.constraint_top = nil end
        if edge_right then self.constraint_right = right else self.constraint_right = nil end
        if edge_bottom then self.constraint_bottom = bottom else self.constraint_bottom = nil end
    end




    function Container:constrain()
        -- NOTE constraints mess with container's x, y, width and height values which are irreversible changes (so cache them before if needed)!
        -- NOTE margins & constraints are also changing the minimum window dimensions

        local w, h, window = love.window.getMode()
        local x = self.x
        local y = self.y
        local width = self.width
        local height = self.height
        local min_width = 1
        local min_height = 1

        if self.background.type == self.SKIN then
            local tex = self.background.scale * vec2(self.background.spritesheet:getDimensions())
            min_width = tex.x
            min_height = tex.y
        end


        -- position and resize the container dictated by constraint values

        if self.constraint_left then
            if self.constraint_right then
                width = math.max(min_width, w - (w * self.constraint_left + w * self.constraint_right))
            end
            x = w * self.constraint_left + self.pivot.x * width
        end

        if self.constraint_right and not self.constraint_left then
            x = w * (1 - self.constraint_right) + self.pivot.x * width - width
        end

        if self.constraint_top then
            if self.constraint_bottom then
                height = math.max(min_height, h - (h * self.constraint_top + h * self.constraint_bottom))
            end
            y = h * self.constraint_top + self.pivot.y * height
        end

        if self.constraint_bottom and not self.constraint_top then
            y = h * (1 - self.constraint_bottom) + self.pivot.y * height - height
        end


        -- (override) previous constraints by higher prioritized margin values

        -- NOTE because skins are rendered the container's size can never be smaller than (1,1)
        -- and that's also why the window must be at least the size of (1,1)

        if self.margin_left then
            if self.margin_right then
                width = math.max(min_width, w - (self.margin_left + self.margin_right))
            end
            x = self.margin_left + self.pivot.x * width
        end

        if self.margin_right and not self.margin_left then
            x = w - self.margin_right + self.pivot.x * width - width
        end

        if self.margin_top then
            if self.margin_bottom then
                height = math.max(min_height, h - (self.margin_top + self.margin_bottom))
            end
            y = self.margin_top + self.pivot.y * height
        end

        if self.margin_bottom and not self.margin_top then
            y = h - self.margin_bottom + self.pivot.y * height - height
        end


        -- update container with new calculations
        
        self.x = x
        self.y = y
        self.width = width
        self.height = height


        -- NOTE when any two opposite margins become larger than the window dimensions (e.g. resize window down) then
        -- the container gets pushed out to the right/bottom so that at least one margin gets ignored!

        -- fix window overflowing issue
        -- restrict minimum window size before two opposite margins overgrow it
        local margin_horizontal = (self.margin_left or 0) + (self.margin_right or 0)
        local margin_vertical = (self.margin_top or 0) + (self.margin_bottom or 0)
        local clamp_dx = math.min(0, w - margin_horizontal)
        local clamp_dy = math.min(0, h - margin_vertical)
        min_width = math.max(window.minwidth, margin_horizontal + min_width)
        min_height = math.max(window.minheight, margin_vertical + min_height)

        if clamp_dx < 0
        or clamp_dy < 0
        or window.minwidth < min_width
        or window.minheight < min_height
        then
            window.minwidth = min_width
            window.minheight = min_height
            love.window.setMode(w, h, window)
        end
    end




    function Container:center()
        local w, h = love.window.getMode()
        local cx, cy = w/2, h/2
        self.x = cx - self.width/2 + self.pivot.x * self.width
        self.y = cy - self.height/2 + self.pivot.y * self.height
    end




    function Container:addChild(entity)
        -- NOTE child components have relative position to this parent
        -- This also the reason why we need to override their *.hit() method!
    end




    function Container:drawChildren()
        -- TODO draw each child component
    end




    function Container:drawBackground(rect_color, rect_radius)
        rect_color = rect_color or self.background.color
        rect_radius = clamp(rect_radius or self.background.radius, 0, self.width/4)
        love.graphics.push("all")
        love.graphics.setColor(rect_color:unpack())
        love.graphics.rectangle("fill", 0, 0, self.width, self.height, rect_radius, rect_radius)
        love.graphics.pop()
    end




    function Container:drawSkin(...)
        if not self.background.texture
        or self.background.texture:getWidth() ~= self.width
        or self.background.texture:getHeight() ~= self.height
        then
            self:render(...)
        end
        love.graphics.draw(self.background.texture)
    end




    function Container:draw()
        if self.visible then
            self:constrain()
            love.graphics.push("all")
            love.graphics.translate(self.x - self.pivot.x * self.width, self.y - self.pivot.y * self.height)
            self.background.type(self) -- draw reference call
            self:drawChildren()
            love.graphics.pop()
        end
    end




    function Container:render(spritesheet, tile_width, tile_height, scale)
        -- NOTE if you change the scale value at runtime you'll have to invoke the re-rendering process manually!
        -- however changing self.width and self.height gets considered by the renderer

        spritesheet = spritesheet or self.background.spritesheet
        tile_width = tile_width or self.background.tile_width
        tile_height = tile_height or self.background.tile_height
        scale = scale or self.background.scale

        assert(
            spritesheet
            and tile_width
            and tile_height
            and scale,
            "Entity is missing skin properties"
        )

        local x = 0
        local y = 0
        local w = self.width / scale
        local h = self.height / scale
        local _, tile = uvTexture(spritesheet, tile_width, tile_height, 1, 9, true)
        local quad = love.graphics.newQuad(0, 0, 0, 0, spritesheet:getDimensions())
        
        local shape = {
            -- 3x3 tiles matrix
            -- x,            y,               w,                h
            -- row 1
            {0,              0,               tile_width,       tile_height},
            {tile_width,     0,               w - 2*tile_width, tile_height},
            {w - tile_width, 0,               tile_width,       tile_height},
            -- row 2
            {0,              tile_height,     tile_width,       h - 2*tile_height},
            {tile_width,     tile_height,     w - 2*tile_width, h - 2*tile_height},
            {w - tile_width, tile_height,     tile_width,       h - 2*tile_height},
            -- row 3
            {0,              h - tile_height, tile_width,       tile_height},
            {tile_width,     h - tile_height, w - 2*tile_width, tile_height},
            {w - tile_width, h - tile_height, tile_width,       tile_height}
        }

        self.background.texture = love.graphics.newCanvas(self.width, self.height)
        self.background.texture:setFilter("linear", "nearest")
        spritesheet:setFilter("linear", "nearest")
        spritesheet:setWrap("repeat", "repeat")

        love.graphics.push("all")
        love.graphics.reset()
        love.graphics.scale(scale, scale)
        love.graphics.setCanvas(self.background.texture)
        love.graphics.setColor(self.background.color:unpack())

        for i, part in ipairs(tile) do
            x, y, w, h = unpack(shape[i])
            local sx = w / tile[i].width
            local sy = h / tile[i].height
            quad:setViewport(tile[i].x, tile[i].y, tile[i].width, tile[i].height)
            love.graphics.draw(spritesheet, quad, x, y, 0, sx, sy)
        end

        love.graphics.setCanvas()
        love.graphics.pop()
    end




    function Container:hit(x, y)
    end
    



































    





    Text = class()
    

    -- presets
    Text.FONT_FAMILY = "jingleberry.otf"
    Text.FONT_LINE_HEIGHT = .75
    Text.FONT_LARGE = 38
    Text.FONT_MEDIUM = 24
    Text.FONT_SMALL = 18



    function Text:init(title, x, y)
        self.x = x or 0
        self.y = y or 0
        self.pivot = vec2()
        self.title = title or ""
        self.format = "%s"
        self.align = "left"
        self.wrap = wrap_width or love.graphics.getWidth()
        self.color = color(68, 128, 223)
        self.visible = true

        self:setFont()
    end


    function Text:setFont(font_family, letter_height, line_height)
        -- NOTE löve2d seems to generate a spritesheets from font files so we need to re-read on any changes
        self.font = love.graphics.newFont(font_family or self.FONT_FAMILY, letter_height or self.FONT_MEDIUM)
        self.font:setLineHeight(line_height or self.FONT_LINE_HEIGHT)
    end


    function Text:getFont()
        local font_family = self.font
        local font_line_height = self.font:getLineHeight()
        local font_size = self.font:getHeight() * font_line_height
        return
            font_family,
            font_size,
            font_line_height
    end


    function Text:getDimensions()
        local w = self.font:getWidth(string.format(self.format, self.title))
        local h = self.font:getHeight()
        local width = self.wrap
        local height = math.ceil(w / self.wrap) * h
        return width, height
    end


    function Text:draw()
        if self.visible then
            local w, h = self:getDimensions()
            love.graphics.push("all")
            love.graphics.translate(self.x - self.pivot.x * w, self.y - self.pivot.y * h)
            love.graphics.setFont(self.font)
            love.graphics.setColor(self.color:unpack())
            love.graphics.printf(string.format(self.format, self.title), 0, 0, self.wrap, self.align)
            love.graphics.pop()
        end
    end





































 
    Button = class()


    function Button:init(title, x, y, width, height, callback)
        self.x = x or 0
        self.y = y or 0
        self.pivot = vec2()
        self.width = width or 80
        self.height = height or 48
        self.radius = 4
        self.callback = callback
        self.hovered = false
        self.triggered = false
        self.visible = true
        self.bg_color = color(40)
        self.bg_hover_color = color(68, 128, 223)
        self.bg_trigger_color = color(131, 118, 156)

        -- center text on button
        self.label = Text(title)
        self.label.x = self.width/2 -- position relative to the parent
        self.label.y = self.height/2
        self.label.pivot = vec2(.5, .5)
        self.label.wrap = self.width
        self.label.align = "center"
        self.label.hover_color = color(255)
        self.label.trigger_color = color(40)
    end


    function Button:setDimensions(width, height)
        -- NOTE use this function when resizing buttons but want to keept text centered
        local dx = (width - self.width) / 2
        local dy = (height - self.height) / 2
        self.width = width
        self.height = height
        self.label.x = self.label.x + dx
        self.label.y = self.label.y + dy
        self.label.wrap = self.width
    end


    function Button:drawBackground()
        local r = clamp(self.radius, 0, self.width/2)
        local c = self.bg_color

        if self.hovered then
            c = self.bg_hover_color
        end
        
        if self.triggered then
            c = self.bg_trigger_color
        end

        love.graphics.setColor(c:unpack())
        love.graphics.rectangle("fill", 0, 0, self.width, self.height, r, r)
    end


    function Button:drawTitle()
        if self.label then
            local c = self.label.color -- cache

            -- use a little hack to modify text color on different states
            if self.hovered then
                self.label.color = self.label.hover_color
            end
            
            if self.triggered then
                self.label.color = self.label.trigger_color
            end

            self.label:draw()
            self.label.color = c -- restore
        end
    end


    function Button:draw()
        if self.visible then
            love.graphics.push("all")
            love.graphics.translate(self.x - self.pivot.x * self.width, self.y - self.pivot.y * self.height)
            self:drawBackground()
            self:drawTitle()
            love.graphics.pop()
        end
    end


    function Button:hit(src_x, src_y)
        local left = self.x - self.pivot.x * self.width
        local top = self.y - self.pivot.y * self.height
        local right = left + self.width
        local bottom = top + self.height
        return src_x > left and src_x < right and src_y > top and src_y < bottom
    end


    function Button:touched(touch)
        --if self.visible then -- might be an invisible in-game trigger so keep listening for touches
            if touch.state == "pressed" and self:hit(touch.x, touch.y) then
                self.triggered = true
            end

            if touch.state == "released" then
                self.triggered = false
            end

            if self:hit(touch.x, touch.y) then
                self.hovered = true
            else
                self.hovered = false
            end

            if self.callback
            and touch.state == "released"
            and self:hit(touch.init_x, touch.init_y)
            and self:hit(touch.x, touch.y)
            then
                self.callback()
            end
        --end
    end




























    Alert = class()


    function Alert:init(message, x, y, width, height)
        local cx, cy = percentPositionToPixel(.5, .5)
        local w, h = percentPositionToPixel(.75, .5)

        self.x = x or cx
        self.y = y or cy
        self.pivot = vec2(.5, .5)
        self.width = width or w
        self.height = height or h
        self.radius = 4
        self.bg_color = color(40)
        self.visible = false

        -- NOTE components have relative position to the parent
        -- This also the reason why we need to override the Button.hit method!

        -- left button
        self.btn_left = Button("Cancel")
        self.btn_left:setDimensions(self.width/2, self.btn_left.height)
        self.btn_left.radius = self.radius
        self.btn_left.pivot = vec2(0, 1)
        self.btn_left.callback = function() self:dismiss() end
        self.btn_left.hit = function(this, src_x, src_y)
            local parent = vec2(self.x - self.pivot.x * self.width, self.y - self.pivot.y * self.height)
            local btn = parent + vec2(this.x - this.pivot.x * this.width, this.y - this.pivot.y * this.height)
            return src_x > btn.x and src_x < btn.x + this.width and src_y > btn.y and src_y < btn.y + this.height
        end

        -- right button
        self.btn_right = Button("Ok")
        self.btn_right:setDimensions(self.width/2, self.btn_right.height)
        self.btn_right.radius = self.radius
        self.btn_right.pivot = vec2(1, 1)
        self.btn_right.callback = function() self:dismiss() end
        self.btn_right.hit = function(this, src_x, src_y)
            local parent = vec2(self.x - self.pivot.x * self.width, self.y - self.pivot.y * self.height)
            local btn = parent + vec2(this.x - this.pivot.x * this.width, this.y - this.pivot.y * this.height)
            return src_x > btn.x and src_x < btn.x + this.width and src_y > btn.y and src_y < btn.y + this.height
        end

        -- center message text on window
        self.message = Text(message)
        self.message.wrap = self.width
        self.message.pivot = vec2(.5, .5)
        self.message.align = "center"

        -- display the window by default
        self:clampDimensions()
        self:center()
        self:open()
    end


    function Alert:open()
        -- NOTE in an actual case implimentation you should maybe override this method and freeze the gameplay
        self.visible = true
    end


    function Alert:dismiss()
        self.visible = false
    end


    function Alert:clampDimensions(width, height)
        -- NOTE implemented this method with mobile devices in mind when these change orientation (portrait, landscape)
        local w, h = love.window.getMode()
        width = width or w
        height = height or h

        if self.width > width or self.height > height then
            self.width = clamp(self.width, 0, width)
            self.height = clamp(self.height, 0, height)
            self.message.wrap = self.width

            if self.btn_left.width > self.width/2 then
                self.btn_left:setDimensions(self.width/2, self.btn_left.height)
            end

            if self.btn_right.width > self.width/2 then
                self.btn_right:setDimensions(self.width/2, self.btn_right.height)
            end
        end
    end


    function Alert:center()
        -- re-center self and all components
        local width, height = love.window.getMode()
        self.x = width/2
        self.y = height/2
        self.btn_left.x = 0
        self.btn_left.y = self.height
        self.btn_right.x = self.width
        self.btn_right.y = self.height
        self.message.x = self.width/2
        self.message.y = (self.height - self.btn_left.height) / 2
    end


    function Alert:drawBackground()
        local r = clamp(self.radius, 0, self.width/2)
        love.graphics.setColor(self.bg_color:unpack())
        love.graphics.rectangle("fill", 0, 0, self.width, self.height, r, r)
    end


    function Alert:draw()
        if self.visible then
            love.graphics.push("all")
            love.graphics.translate(self.x - self.pivot.x * self.width, self.y - self.pivot.y * self.height)
            self:drawBackground()
            self.message:draw()
            self.btn_left:draw()
            self.btn_right:draw()
            love.graphics.pop()
        end
    end


    function Alert:touched(touch)
        if self.visible then
            self.btn_left:touched(touch)
            self.btn_right:touched(touch)
        end
    end


end -- UI BLOCK


