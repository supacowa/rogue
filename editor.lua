

-- TODO currently the in-game editor only works on MacOS and is a dev tool only!
-- Consider making it an easter egg and be available on all supported platforms


local SECRET = "god" -- passcode to open the editor
local OPENED = false -- just a flag
local FEATURE_LOCK = false -- TODO replace through config.development_mode!











function openEditor()
    -- TODO set TIME_SCALE to zero
    OPENED = true
end





function closeEditor()
    -- TODO alert to save changes before exit
    OPENED = false
end





function drawEditor()
    if OPENED then
        love.graphics.print("Editor. Welcome.")
    end
end













-- This listener will open the in-game editor
-- Just type in the preset pass-phrase (secret) without any mistakes and within a certain time frame

do
    local time = .5
    local expiration, input, cursor


    function updateEditorPasscodeListener(with_key)
        if OPENED or FEATURE_LOCK then
            return
        end

        -- reset
        if love.timer.getTime() > (expiration or 0) then
            cursor = 0
            input = ""
        end

        expiration = time + love.timer.getTime()

        -- validate input
        if #input < #SECRET then
            if SECRET:sub(cursor + 1, cursor + 1) == with_key then
                -- continue
                input = input .. with_key
                cursor = cursor + 1
            else
                -- retry
                cursor = 0
                input = ""
            end
        end

        -- accept secret and open editor
        if input == SECRET then
            openEditor()
        end
    end
end








-- This listener will close the in-game editor respectivly

function updateEditorShutdownListener(with_key)
    if OPENED and with_key == "q" then
        closeEditor()
    end
end

