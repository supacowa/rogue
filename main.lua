
-- NOTE libraries that are defined inside the config file will be available automatically here! (This because conf.lua is loaded by löve2d.)



function love.load()
    group = Container(0, 0, 140, 48)
    --group.background.color = color(60)
    group.background.color = color(40, 70, 110)
    group.background.type = group.SKIN
    group.pivot = vec2(.5, .5)
    group:center()

    label = Text("Settings", group.x, group.y)
    --label:setFont(nil, label.FONT_LARGE, nil)
    label.align = "center"
    label.pivot = vec2(.5, .5)
end










function love.draw()
    group:draw()
    label:draw()
    drawEditor()
    debugger()
end









function love.keyreleased(key)
    updateEditorPasscodeListener(key)
    updateEditorShutdownListener(key)
end
















-- NOTE  USE THIS METHOD TO DISPATCH CLICKS AND TOUCHES TO GAME OBJECTS!
-- Use this main dispatcher callback because now all mouse clicks and touches are unified

function touched(touch_queue, current_touch)
    local touch = touch_queue[current_touch]

end





















-- NOTE  DO NOT TOUCH THE FALLOWING CODE!

-- NOTE löve2d forwards mouse events to single touch callbacks
-- So, unify all click and touch events into one single emitter object - the 'touch' object
-- and then dispatch all events to responsible game objects

do
    -- Override löve2d default behaviour of treating mouse and touch events separately

    local touch_queue = {}
    local CURRENT_TOUCH = 0





    -- Names of all available event states

    local TOUCH_STATE = {
        pressed = "pressed",
        moved = "moved",
        scrolled = "scrolled",
        released = "released"
    }





    -- Register, update and dispatch touches

    local function trackTouch(id, state, x, y, dx, dy, scroll_x, scroll_y, button, pressure, is_mouse)

        -- cache current touch reference
        CURRENT_TOUCH = #touch_queue + 1

        -- identify touch
        for i, t in ipairs(touch_queue) do
            if t.id == id then
                CURRENT_TOUCH = i -- update cached reference
                break
            end
        end

        -- prepare template
        local t = touch_queue[CURRENT_TOUCH] or {}
        local tpl = {
            id = id,
            state = state,
            is_mouse = is_mouse,
            init_x = state == TOUCH_STATE.pressed and x or (t.init_x or x),
            init_y = state == TOUCH_STATE.pressed and y or (t.init_y or y),
            x = x,
            y = y,
            delta_x = dx or 0,
            delta_y = dy or 0,
            scroll_x = scroll_x or 0,
            scroll_y = scroll_y or 0,
            button = button or t.button or 0,
            pressure = pressure or 1
        }

        -- update touch with template
        touch_queue[CURRENT_TOUCH] = tpl
        
        -- dispatch touches
        --touched(touch_queue, CURRENT_TOUCH) -- NOTE this would reference the hidden stack directly and programmer could potentically mess it up so we clone the queue before passing on
        touched(cloneTable(touch_queue), CURRENT_TOUCH)

        -- restore correct button state after released state
        if touch_queue[CURRENT_TOUCH].state == TOUCH_STATE.released then
            touch_queue[CURRENT_TOUCH].button = 0
        end
    end





    -- De-register touches

    local function discardTouch(id)
        if isNumber(id) then
            -- identify touch
            for i, t in ipairs(touch_queue) do
                if t.id == id then
                    -- delete
                    table.remove(touch_queue, id)
                    CURRENT_TOUCH = clamp(#touch_queue, 0, #touch_queue) -- current touch is always the latest in the queue
                    return
                end
            end
        end
    end





    -- Reroute touches to the main dispatcher
    -- NOTE we don't dispatch mouse events when they came from a touch screen (is_touch) because their responsible callbacks get called anyway

    function love.mousepressed(x, y, button, is_touch)
        if not is_touch then
            trackTouch("mouse", TOUCH_STATE.pressed, x, y, 0, 0, 0, 0, button, 1, not is_touch)
        end
    end

    function love.mousemoved(x, y, dx, dy, is_touch)
        -- NOTE this is actually the first callback that will register the "mouse" touch object because before the mouse clicked it touched the screen already
        if not is_touch then
            trackTouch("mouse", TOUCH_STATE.moved, x, y, dx, dy, 0, 0, nil, 1, not is_touch)
        end
    end

    function love.mousereleased(x, y, button, is_touch)
        if not is_touch then
            trackTouch("mouse", TOUCH_STATE.released, x, y, 0, 0, 0, 0, button, 1, not is_touch)
        end
    end

    function love.wheelmoved(x, y)
        -- positive x is to the right
        -- positive y is upward
        -- NOTE both values are inverted on MacOS with "natural scrolling direction" being activated!
        trackTouch("mouse", TOUCH_STATE.scrolled, 0, 0, 0, 0, x, y, nil, 1)
    end

    function love.touchpressed(id, x, y, dx, dy, pressure)
        trackTouch(id, TOUCH_STATE.pressed, x, y, dx, dy, 0, 0, nil, pressure, true)
    end

    function love.touchmoved(id, x, y, dx, dy, pressure)
        trackTouch(id, TOUCH_STATE.pressed, x, y, dx, dy, 0, 0, nil, pressure, true)
    end

    function love.touchreleased(id, x, y, dx, dy, pressure)
        trackTouch(id, TOUCH_STATE.released, x, y, dx, dy, 0, 0, nil, pressure, true)
        discardTouch(id)
    end

end
