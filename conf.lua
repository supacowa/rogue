
-- NOTE you can require this file and receive a table all config setting (löve and custom as well)
-- Additional settings, configs and global constants used by the game go here

local config = {
    developer_mode = true -- NOTE deactivate this flag for distribution builds of the game!
}






-- Load frequently used libraries in this place, others when and where needed. (Preferably, these libs are global namespaces.)
-- NOTE some libraries are only needed for development so put them under the if-check!

require "helpers"

if config.developer_mode then
    require "console"
    require "editor"
end






function love.conf(t)
    t.identity = "rogue" -- save directory name
    t.version = "0.11.0" -- min love2d version
 
    t.window.title = "Rogue"
    t.window.width = 640
    t.window.height = 480
    t.window.borderless = false
    t.window.resizable = true
    t.window.minwidth = 160
    t.window.minheight = 120
    t.window.fullscreen = false
    t.window.fullscreentype = "desktop"

    t.gammacorrect = true
    t.window.vsync = true
    t.window.msaa = 8
    t.window.display = 1
    t.window.highdpi = false -- retina
 
    t.modules.audio = true
    t.modules.sound = true
    t.modules.window = true -- important
    t.modules.image = true
    t.modules.event = true
    t.modules.system = true
    t.modules.thread = true
    t.modules.physics = true
    t.modules.math = true
    t.modules.timer = true
    t.modules.graphics = true -- important
    t.modules.keyboard = true
    t.modules.mouse = true
    t.modules.touch = true

    cloneTable(t, config) -- make this stuff also available to the game
end



return config
